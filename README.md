# User
========================

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist exoo/yii2-user
```

or add

```json
"exoo/yii2-user": "*"
```

to the require section of your composer.json.



Usage
-----

Example module configuration:

```
'modules' => [
    'user' => [
        'class' => 'exoo\user\Module',
        'useRecaptcha' => true,
        'recaptchaOptions' => $params['recaptcha'],
        'robotEmail' => 'no-reply@exoodev.com',
        'urlPrivacyPolicy' => '/agreement/privacy-policy',
    ]
],
```

Example component configuration:

```
'components' => [
    'user' => [
        'identityClass' => 'exoo\user\models\User',
        'enableAutoLogin' => true,
        'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
    ],
    'fileStorage' => [
        'class' => 'exoo\storage\FileStorage',
        'storage' => [
            'class' => 'yii2tech\filestorage\local\Storage',
            'basePath' => '@storage',
            'dirPermission' => 0775,
            'filePermission' => 0755,
        ],
    ],
],
```