<?php

namespace exoo\user\controllers\frontend;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use exoo\user\models\LoginForm;
use exoo\user\models\frontend\forms\SignupForm;
use exoo\user\components\AuthHandler;
use exoo\uikit\ActiveForm;
use exoo\user\models\frontend\forms\ResendEmailConfirmForm;
use yii\web\BadRequestHttpException;
use yii\base\InvalidParamException;
use exoo\user\models\frontend\forms\PasswordResetRequestForm;
use exoo\user\models\frontend\forms\ResetPasswordForm;
use exoo\user\models\frontend\forms\EmailConfirmForm;
use yii\filters\AjaxFilter;

/**
 * AuthController.
 */
class AuthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'logout',
                    'signup',
                    'resend-activation',
                    'request-password-reset',
                    'add-email',
                ],
                'rules' => [
                    [
                        'actions' => [
                            'signup',
                            'resend-activation',
                            'request-password-reset',
                            'add-email',
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            [
                'class' => AjaxFilter::className(),
                'only' => ['remove-client']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'client' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
        return $this->goBack();
    }

    /**
     * Login action.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                return $this->asJson(ActiveForm::validate($model));
            }
            if ($user = $model->signup()) {
                if ($this->module->emailConfirm) {
                    $model->sendEmailConfirm($user);
                    return $this->redirect(['activation', 'email' => $model->email]);
                }
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Activation page
     *
     * @param null|string $email
     * @return mixed
     */
    public function actionActivation($email = null)
    {
        return $this->render('activation', [
            'email' => $email,
        ]);
    }

    /**
     * Resend email confirm token page.
     * @param null|string $email
     * @return mixed
     */
    public function actionResendActivation($email = null)
    {
        $model = new ResendEmailConfirmForm();
        $model->email = $email;

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                return $this->asJson(ActiveForm::validate($model));
            }
            if ($model->resend()) {
                return $this->redirect(['activation', 'email' => $model->email]);
            } else {
                return $this->refresh();
            }
        }
        return $this->render('resendActivation', [
            'model' => $model,
        ]);
    }

    /**
     * Add e-mail.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionAddEmail($token)
    {
        try {
            $model = new AddEmailForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->addEmail()) {
                if (Yii::$app->settings->get('system', 'emailConfirm')) {
                    $user->sendMailConfirm();
                    return $this->redirect(['activation', 'email' => $model->email]);
                }
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('addEmail', [
            'model' => $model,
        ]);
    }

    /**
     * Confirm e-mail.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionEmailConfirm($token)
    {
        try {
            $model = new EmailConfirmForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->confirmEmail()) {
            Yii::$app->getSession()->setFlash('alert.success', Yii::t('user', 'Your e-mail successfully confirmed.'));
        } else {
            Yii::$app->getSession()->setFlash('alert.error', Yii::t('user', 'Sorry, we are unable to verify your account with provided token.'));
        }

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {
            Yii::$app->session->setFlash('alert.success', Yii::t('user', 'Check your e-mail for further instructions.'));
            return $this->redirect(['login']);
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {
            Yii::$app->session->setFlash('alert.success', Yii::t('user', 'New password saved.'));

            return $this->redirect(['login']);
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionRemoveClient($client)
    {
        $user = Yii::$app->user->identity;

        foreach ($user->auths as $auth) {
            if ($auth->source == $client) {
                if ($auth->delete()) {
                    return $this->asJson(['result' => true]);
                }
           }
        }
    }
}