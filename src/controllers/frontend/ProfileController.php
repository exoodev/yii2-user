<?php

namespace exoo\user\controllers\frontend;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use exoo\user\models\frontend\forms\ProfileForm;

class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * User profile.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        $model = new ProfileForm($user);
        Yii::$app->user->returnUrl = Yii::$app->request->url;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('user', 'Saved'));
            return $this->refresh();
        }

        return $this->render('index', [
            'model' => $model,
            'user' => $user,
        ]);
    }
}