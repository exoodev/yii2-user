<?php

namespace exoo\user\assets;

use yii\web\AssetBundle;

class UserStyleAsset extends AssetBundle
{
    public $sourcePath = '@exoo/user/assets';
    public $css = [
        'css/user.css',
    ];
}