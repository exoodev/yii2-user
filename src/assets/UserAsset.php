<?php

namespace exoo\user\assets;

/**
 * Asset bundle.
 */
class UserAsset extends \yii\web\AssetBundle
{
    /**
     * {@inheritdoc}
     */
    public $sourcePath = '@exoo/user/assets';
    /**
     * {@inheritdoc}
     */
    public $js = [
        'js/user.js',
    ];
    /**
     * {@inheritdoc}
     */
    public $depends = [
    	'yii\web\JqueryAsset',
    ];
}