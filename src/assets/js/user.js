var User = {
    init: function () {
        var _this = this

        $(document).on('click', '.auth-item.active', function (e) {
            e.preventDefault()
        })
        $(document).on('click', '.auth-remove', function () {
            var el = $(this)
            var client = el.data('client')
            $.get('/user/auth/remove-client', { client }).done(function (data) {
                if (data.result) {
                    el.closest('.active').removeClass('active')
                }
            })
        })
    },
    loginMask: function (masks) {
        var _this = this
        var switcher = $('[user-login] [uk-switcher]')

        if (switcher.length) {
            switcher.on('click', function () {
                _this._setLoginMask(switcher, masks)
            })

            this._setLoginMask(switcher, masks)
        }
    },
    setPassword: function (event) {
        /** @todo button show password */
        event.preventDefault()
        var $input = $(event.target).closest('div').children('input')
        
        if ($input.length) {
            $input
                .val(this._generatePassword(8))
                .prop('type', 'text')
                .on('input change', function() {
                    if (!this.value) {
                        this.type = 'password'
                    }
                })
        }
    },
    _generatePassword: function(length) {
        var password = ''
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890"

        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length)
            password += chars.charAt(i)
        }

        return password
    },
    _setLoginMask: function (switcher, masks) {
        var item = switcher.find('.uk-active a')
        var input = $('.js-user-entity')

        switch (item.data('name')) {
            case 'phone':
                input
                    .attr({ type: 'tel', autocomplete: 'tel' })
                    .inputmask({ mask: masks, alias: 'phone' })
                break;
            case 'email':
                input
                    .attr({ type: 'email', autocomplete: 'email' })
                    .inputmask('email')
                break;
            default:
                input
                    .attr({ type: 'text', autocomplete: 'off' })
                    .inputmask('remove')
                break;
        }
        input.attr('placeholder', item.text()).val('')
    }
}
User.init()