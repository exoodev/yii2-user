<?php

namespace exoo\user\traits;

use Yii;
use exoo\user\Module;

/**
 * Module trait
 */
trait ModuleTrait
{
    /**
     * @var Module|null Module instance
     */
    private $_module;

    /**
     * @return Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $module = Module::getInstance();

            if ($module instanceof Module) {
                $this->_module = $module;
            } else {
                $this->_module = Yii::$app->getModule('user');
            }
        }

        return $this->_module;
    }
}