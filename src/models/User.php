<?php

namespace exoo\user\models;

use Yii;
use exoo\user\models\queries\UserQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use exoo\user\traits\ModuleTrait;
use exoo\user\Module;
use exoo\user\helpers\PhoneHelper;
use yii\db\ActiveQuery;
use yii2tech\authlog\AuthLogIdentityBehavior;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $phone
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $last_visit
 * @property string $password write-only password
 * 
 * @property UserProfile $profile
 * @property Auth $auths
 * 
 */
class User extends ActiveRecord implements IdentityInterface
{
    use ModuleTrait;

    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;
    const STATUS_BLOCKED = 20;

    const EVENT_REGISTER = 'register';

    /**
     * @var string
     */
    public $password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'authLog' => [
                'class' => AuthLogIdentityBehavior::className(),
                'authLogRelation' => 'authLogs',
                'defaultAuthLogData' => function ($model) {
                    return [
                        'ip' => Yii::$app->request->getUserIP(),
                        'host' => @gethostbyaddr(Yii::$app->request->getUserIP()),
                        'url' => Yii::$app->request->getAbsoluteUrl(),
                        'user_agent' => Yii::$app->request->getUserAgent(),
                    ];
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                'status',
                'default',
                'value' => $this->module->emailConfirm ? self::STATUS_INACTIVE : self::STATUS_ACTIVE
            ],
            ['status', 'in', 'range' => [
                self::STATUS_DELETED,
                self::STATUS_INACTIVE,
                self::STATUS_ACTIVE,
                self::STATUS_BLOCKED,
            ]],
            ['phone', 'filter', 'filter' => function($value) {
                return PhoneHelper::format($value);
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user', 'Username'),
            'password' => Yii::t('user', 'Password'),
            'email' => 'E-mail',
            'phone' => Yii::t('user', 'Phone'),
            'status' => Yii::t('user', 'Status'),
            'created_at' => Yii::t('user', 'Registration date'),
            'updated_at' => Yii::t('user', 'Date of change'),
            'last_visit' => Yii::t('user', 'Last visit'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @return ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAuths()
    {
        return $this->hasMany(Auth::className(), ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAuthLogs()
    {
        return $this->hasMany(UserAuthlog::className(), ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::find()->where(['id' => $id])->active()->one();
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by name.
     *
     * @param string $name
     * @return static|null
     */
    public static function findByName($name)
    {
        return static::find()->where(['username' => $name])->active()->one();
    }

    /**
     * Finds user by email.
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::find()->where(['email' => $email])->active()->one();
    }

    /**
     * Finds user by phone.
     *
     * @param string $phone
     * @return static|null
     */
    public static function findByPhone($phone)
    {
        return static::find()
            ->where(['phone' => preg_replace('/\D+/', '', $phone)])
            ->active()
            ->one();
    }

    /**
     * Finds user by password reset token.
     *
     * @param string $token password reset token.
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::find()->where(['password_reset_token' => $token])->active()->one();
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Generates email verification token.
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes email verification token.
     */
    public function removeEmailVerificationToken()
    {
        $this->verification_token = null;
    }

    /**
     * Finds out if password reset token is valid.
     *
     * @param string $token password reset token.
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Module::getInstance()->passwordResetTokenExpire;
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate.
     * @return boolean if password provided is valid for current user.
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model.
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key.
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token.
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return boolean
     */
    public function isNotDeleted()
    {
        return $this->status !== self::STATUS_DELETED;
    }

    public function asActive()
    {
        $this->status = self::STATUS_ACTIVE;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @return boolean
     */
    public function isInActive()
    {
        return $this->status === self::STATUS_INACTIVE;
    }

    /**
     * Returns the roles that are assigned to the user.
     * @return array the result
     */
    public function getRoles()
    {
        return array_keys(Yii::$app->authManager->getRolesByUser($this->id));
    }

    /**
     * Assign role for user
     * @param string $role
     */
    public function assignRole($role)
    {
        $auth = Yii::$app->authManager;
        $authRole = $auth->getRole($role);
        $auth->assign($authRole, $this->id);
    }
}
