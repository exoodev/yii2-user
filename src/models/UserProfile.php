<?php

namespace exoo\user\models;

use Yii;

/**
 * This is the model class for table "{{%user_profile}}".
 *
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property int $gender
 * @property string $photo
 * @property string $birthday
 * @property string $country
 * @property string $city
 * @property string $address
 * @property string $timezone
 *
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
	const GENDER_FEMALE = 1;
    const GENDER_MALE = 2;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'gender'], 'integer'],
            [['birthday'], 'safe'],
            [['timezone'], 'string', 'max' => 100],
            [['photo', 'country', 'city', 'address', 'first_name', 'last_name'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('user', 'User'),
            'gender' => Yii::t('user', 'Gender'),
            'first_name' => Yii::t('user', 'First Name'),
            'last_name' => Yii::t('user', 'Last Name'),
            'photo' => Yii::t('user', 'Photo'),
            'birthday' => Yii::t('user', 'Birthday'),
            'country' => Yii::t('user', 'Country'),
            'city' => Yii::t('user', 'City'),
            'address' => Yii::t('user', 'Address'),
            'timezone' => Yii::t('user', 'Timezone'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
