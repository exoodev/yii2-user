<?php

namespace exoo\user\models\backend;

use Yii;
use yii\base\Model;
use exoo\user\models\User;
use exoo\user\traits\ModuleTrait;
use exoo\user\helpers\PhoneHelper;

/**
 * User form.
 */
class UserForm extends Model
{
    use ModuleTrait;

    const SCENARIO_CREATE = 'create';
    
    public $username;
    public $email;
    public $phone;
    public $password;
    public $status;

    private $_user;

    public function __construct(User $user = null, $config = [])
    {
        if ($user) {
            $this->username = $user->username;
            $this->email = $user->email;
            $this->phone = $user->phone;
            $this->status = $user->status;
            $this->_user = $user;
        } else {
            $this->_user = new User();
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['phone', 'trim'],
            ['phone', 'string', 'max' => 20],
            ['phone', 'filter', 'filter' => function($value) {
                return PhoneHelper::format($value);
            }],
            [
                'phone',
                'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('user', 'This phone is already in use.'),
                'filter' => $this->_user->id ? ['<>', 'id', $this->_user->id] : null,
            ],
            ['phone', 'required', 'when' => function() {
                return $this->module->loginPhone;
            }],

            ['password', 'string', 'min' => 6, 'max' => 100],
            ['password', 'required', 'on' => self::SCENARIO_CREATE],
            
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('user', 'This e-mail address has already been taken.'),
                'filter' => $this->email ? ['<>', 'email', $this->email] : null
            ],
            
            ['status', 'default', 'value' => User::STATUS_ACTIVE],
            ['status', 'in', 'range' => [
                User::STATUS_DELETED,
                User::STATUS_INACTIVE,
                User::STATUS_ACTIVE,
                User::STATUS_BLOCKED,
            ]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user', 'Username'),
            'password' => Yii::t('user', 'Password'),
            'email' => 'E-mail',
            'phone' => Yii::t('user', 'Phone'),
            'status' => Yii::t('user', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord || (!$this->isNewRecord && $this->password)) {
                $this->setPassword($this->password);
                $this->generateAuthKey();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Save User model
     *
     * @return bool whether the creating new account was successful
     */
    public function save()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = $this->_user;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->phone = $this->phone;
        $user->status = $this->status;
        
        if ($user->isNewRecord) {
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailVerificationToken();
        } else {
            if ($this->password) {
                $user->setPassword($this->password);
                $user->removePasswordResetToken();
            }
        }

        if ($user->save()) {
            return $user;
        }
    }

    /**
     * Get country codes
     * @return array
     */
    public static function getCountryCodeList()
    {
        return User::getCountryCodeList();
    }
}
