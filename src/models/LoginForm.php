<?php

namespace exoo\user\models;

use Yii;
use yii\base\Model;
use exoo\user\models\User;
use exoo\user\traits\ModuleTrait;

/**
 * Login form.
 */
class LoginForm extends Model
{
    use ModuleTrait;

    public $entity;
    public $password;
    public $rememberMe = true;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'entity' => Yii::t('user', $this->module->loginPhone ? 'Phone or email' : 'E-mail'),
            'phone' => Yii::t('user', 'Phone'),
            'email' => Yii::t('user', 'E-mail'),
            'password' => Yii::t('user', 'Password'),
            'rememberMe' => Yii::t('user', 'Remember me'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $entity = $this->isEmail() ? 'e-mail' : Yii::t('user', 'phone');
                $this->addError($attribute, Yii::t('user', 'Incorrect {entity} or password.', ['entity' => $entity]));
            }
        }
    }

    /**
     * Logs in a user.
     *
     * @return boolean whether the user is logged in successfully.
     */
    public function login()
    {
        if ($this->validate()) {
            $duration = $this->rememberMe ? $this->module->rememberMeDuration : 0;
            $user = $this->getUser();

            if ($user->isInActive()) {
                Yii::$app->controller->redirect(['activation', 'email' => $user->email]);
            }

            if ($user->isActive()) {
                return Yii::$app->user->login($user, $duration);
            }
        }

        return false;
    }

    /**
     * Finds user by [[entity]].
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            if ($this->isEmail()) {
                $type = 'email';
            } elseif ($this->module->loginPhone) {
                $type = 'phone';
                $this->entity = preg_replace('/\D+/', '', $this->entity);
            }

            $user = User::find()->where([$type => $this->entity])->one();

            if ($user !== null) {
                if ($this->module->isBackend) {
                    if (Yii::$app->authManager->checkAccess($user->id, $this->module->backendPermission)) {
                        $this->_user = $user;
                    }
                } else {
                    $this->_user = $user;
                }
            }
        }

        return $this->_user;
    }

    /**
     * Validate a email
     * 
     * @return boolean
     */
    protected function isEmail()
    {
        return filter_var($this->entity, FILTER_VALIDATE_EMAIL);
    }
}
