<?php

namespace exoo\user\models\queries;

use exoo\user\models\User;

/**
 * This is the ActiveQuery class for [[User]].
 */
class UserQuery extends \yii\db\ActiveQuery
{
    /**
     * Select active.
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => User::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * Select inactive.
     * @return $this
     */
    public function inactive()
    {
        $this->andWhere(['status' => User::STATUS_INACTIVE]);
        return $this;
    }

}
