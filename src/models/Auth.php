<?php

namespace exoo\user\models;

use Yii;

/**
 * This is the model class for table "auth".
 * 
 * @property integer $id
 * @property integer $user_id
 * @property string $source
 * @property string $source_id
 * 
 * @property User $user
 */
class Auth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source', 'source_id'], 'string', 'max' => 255],
            ['user_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => Yii::t('user', 'User'),
            'source' => Yii::t('user', 'Source'),
            'source_id' => Yii::t('user', 'Source ID'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function fields()
    {
        return [
            'source'
        ];
    }
}
