<?php

namespace exoo\user\models;

use Yii;

/**
 * This is the model class for table "{{%user_authlog}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $date
 * @property int $cookie_based
 * @property int $duration
 * @property string $error
 * @property string $ip
 * @property string $host
 * @property string $url
 * @property string $user_agent
 *
 * @property User $user
 */
class UserAuthlog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_authlog}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'date'], 'required'],
            [['user_id', 'date', 'cookie_based', 'duration'], 'integer'],
            [['error', 'host', 'url', 'user_agent'], 'string'],
            [['ip'], 'string', 'max' => 15],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'user_id' => Yii::t('user', 'User'),
            'date' => Yii::t('user', 'Date'),
            'cookie_based' => Yii::t('user', 'Cookie based'),
            'duration' => Yii::t('user', 'Duration'),
            'error' => Yii::t('user', 'Error'),
            'ip' => Yii::t('user', 'Ip'),
            'host' => Yii::t('user', 'Host'),
            'url' => Yii::t('user', 'Url'),
            'user_agent' => Yii::t('user', 'User Agent'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
