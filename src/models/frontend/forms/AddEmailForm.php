<?php

namespace exoo\user\models\frontend\forms;

use Yii;
use yii\base\Model;
use exoo\system\models\User;
use yii\base\InvalidParamException;
use exoo\user\traits\ModuleTrait;

/**
 * Add email form.
 */
class AddEmailForm extends Model
{
    use ModuleTrait;
    
    public $email;

    /**
     * @var User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException(Yii::t('user', 'Email token cannot be blank.'));
        }
        $this->_user = User::findByVerificationToken($token);
        if (!$this->_user) {
            throw new InvalidParamException(Yii::t('user', 'Wrong email token.'));
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email', 'message' => Yii::t('user', 'E-mail is invalid.'),],
            ['email', 'string', 'max' => 255],
            [
                'email', 'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('user', 'This e-mail address has already been taken.'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }

    /**
     * Add email to user.
     * @return User|null the saved model or null if saving fails
     */
    public function addEmail()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->_user;
        $user->email = $this->email;

        if (!$this->module->emailConfirm) {
            $user->removeEmailVerificationToken();
            $user->status = User::STATUS_ACTIVE;
        }

        if ($user->save()) {
            /**
             * @todo need???
             * @todo if neeed to change email message
             */
            // if ($this->module->sendEmailAfterRegistration) {
            //     $this->sendEmailIsRegistered($user);
            // }
            return $user;
        }

        return null;
    }

    /**
     * Send email user is registered
     * @param User $user
     * @return boolean the sending result
     */
    public function sendEmailIsRegistered(User $user)
    {
        return $this->module->mail->compose('registered', ['user' => $user])
            ->setTo($user->email)
            ->setSubject(Yii::t('user', 'Registration on') . '. (' . Yii::$app->name . ')')
            ->send();
    }
}
