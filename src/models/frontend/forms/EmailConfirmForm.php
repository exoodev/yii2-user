<?php

namespace exoo\user\models\frontend\forms;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use exoo\user\models\User;

/**
 * Email confirm form.
 */
class EmailConfirmForm extends Model
{
    /**
     * @var User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     * @param  string $token
     * @param  array $config
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException(Yii::t('user', 'Token confirm e-mail cannot be blank.'));
        }
        $this->_user = User::findByVerificationToken($token);
        if (!$this->_user) {
            throw new InvalidParamException(Yii::t('user', 'Wrong confirm e-mail token.'));
        }
        parent::__construct($config);
    }

    /**
     * Confirm email.
     * @return boolean if email was confirmed.
     */
    public function confirmEmail()
    {
        $user = $this->_user;
        $user->status = User::STATUS_ACTIVE;
        $user->removeEmailVerificationToken();
        Yii::$app->user->login($user);

        return $user->save();
    }
}
