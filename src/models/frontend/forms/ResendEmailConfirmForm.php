<?php

namespace exoo\user\models\frontend\forms;

use Yii;
use yii\base\Model;
use exoo\user\models\User;

/**
 * Resend email confirm form.
 */
class ResendEmailConfirmForm extends Model
{
    /**
     * @var string $email
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'trim'],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'exist',
                'targetClass' => User::className(),
                'filter' => function ($query) {
                    $query->inactive();
                },
                'message' => Yii::t('user', 'E-mail is not found or is already activated'),
            ]
        ];
    }

    /**
     * Resend email confirmation token
     *
     * @return boolean
     */
    public function resend()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::find()->where(['email' => $this->email])->inactive()->one();

        if ($user !== null) {
            $signup = new SignupForm();
            return $signup->sendEmailConfirm($user);
        }

        return false;
    }
}
