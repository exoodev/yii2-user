<?php

namespace exoo\user\models\frontend\forms;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use exoo\user\models\User;
use exoo\user\models\UserProfile;
use exoo\user\traits\ModuleTrait;
use exoo\user\helpers\PhoneHelper;

/**
 * Profile form.
 */
class ProfileForm extends Model
{
    use ModuleTrait;

    public $email;
    public $username;
    public $phone;
    public $first_name;
    public $last_name;
    public $gender;
    public $photo;
    public $birthday;
    public $country;
    public $city;
    public $address;
    public $privacyPolicy = 1;

    private $_user;

    public function __construct(User $user, $config = [])
    {
        if ($user) {
            $this->email = $user->email;
            $this->username = $user->username;
            $this->phone = $user->phone;
            $this->first_name = $user->profile->first_name;
            $this->last_name = $user->profile->last_name;
            $this->gender = $user->profile->gender;
            $this->photo = $user->profile->photo;
            $this->birthday = $user->profile->birthday;
            $this->country = $user->profile->country;
            $this->city = $user->profile->city;
            $this->address = $user->profile->address;
            $this->_user = $user;
        }
        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required', 'when' => function($model) {
                return $model->allowEmailChange();
            }],
            ['email', 'email', 'message' => Yii::t('user', 'E-mail is invalid.')],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('user', 'This e-mail address has already been taken.'),
                'filter' => $this->email ? ['<>', 'email', $this->email] : null
            ],

            ['phone', 'filter', 'filter' => function($value) {
                return PhoneHelper::format($value);
            }],
            ['phone', 'string', 'max' => 20],
            [
                'phone',
                'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('user', 'This phone is already in use.'),
                'filter' => $this->_user->id ? ['<>', 'id', $this->_user->id] : null,
            ],
            ['phone', 'required', 'when' => function() {
                return $this->module->loginPhone;
            }],

            [['gender'], 'integer'],
            [['birthday'], 'safe'],
            [['photo', 'country', 'city', 'address', 'first_name', 'last_name'], 'string', 'max' => 255],
        ];

        if ($this->module->urlPrivacyPolicy) {
            $rules = ArrayHelper::merge($rules, [
                [['privacyPolicy'], 'integer'],
                [
                    'privacyPolicy',
                    'in',
                    'range' => [1],
                    'message' => Yii::t('user', 'Consent is required for the processing of personal data.')
                ]
            ]);
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user', 'Username'),
            'email' => 'E-mail',
            'phone' => Yii::t('user', 'Phone'),
            'gender' => Yii::t('user', 'Gender'),
            'first_name' => Yii::t('user', 'First Name'),
            'last_name' => Yii::t('user', 'Last Name'),
            'photo' => Yii::t('user', 'Photo'),
            'birthday' => Yii::t('user', 'Birthday'),
            'country' => Yii::t('user', 'Country'),
            'city' => Yii::t('user', 'City'),
            'address' => Yii::t('user', 'Address'),
        ];
    }

    public function allowEmailChange()
    {
        return $this->module->allowEmailChange || $this->email === null;
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->_user;
        $user->email = $this->email;
        $user->username = $this->username;
        $user->phone = $this->phone;

        $transaction = $user->getDb()->beginTransaction();

        if ($user->save()) {
            $profile = $user->profile;
            $profile->first_name = $this->first_name;
            $profile->last_name = $this->last_name;
            $profile->gender = $this->gender;
            $profile->photo = $this->photo;
            $profile->birthday = $this->birthday;
            $profile->country = $this->country;
            $profile->city = $this->city;
            $profile->address = $this->address;
            $user->link('profile', $profile);

            $transaction->commit();
            return true;
        }

        return false;
    }
}