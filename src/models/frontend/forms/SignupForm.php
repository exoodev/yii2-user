<?php

namespace exoo\user\models\frontend\forms;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use exoo\user\models\User;
use exoo\user\models\UserProfile;
use exoo\user\traits\ModuleTrait;
use himiklab\yii2\recaptcha\ReCaptchaValidator3;
use exoo\user\helpers\PhoneHelper;

/**
 * Signup form.
 */
class SignupForm extends Model
{
    use ModuleTrait;

    public $username;
    public $email;
    public $phone;
    public $password;
    public $recaptcha;

    public $privacyPolicy = 1;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email', 'message' => Yii::t('user', 'E-mail is invalid.')],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('user', 'This e-mail address has already been taken.'),
            ],

            ['phone', 'filter', 'filter' => function($value) {
                return PhoneHelper::format($value);
            }],
            ['phone', 'string', 'max' => 20],
            [
                'phone',
                'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('user', 'This phone is already in use.'),
            ],
            ['phone', 'required', 'when' => function() {
                return $this->module->loginPhone;
            }],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [
                ['recaptcha'],
                ReCaptchaValidator3::className(),
                'secret' => ArrayHelper::getValue($this->module->recaptchaOptions, 'secretKey'),
                'threshold' => 0.5,
                'action' => null,
                'when' => function() {
                    return $this->module->useRecaptcha;
                }
            ],
        ];

        if ($this->module->urlPrivacyPolicy) {
            $rules = ArrayHelper::merge($rules, [
                [['privacyPolicy'], 'integer'],
                [
                    'privacyPolicy',
                    'in',
                    'range' => [1],
                    'message' => Yii::t('user', 'Consent is required for the processing of personal data.')
                ]
            ]);
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user', 'Name'),
            'password' => Yii::t('user', 'Password'),
            'email' => 'E-mail',
            'phone' => Yii::t('user', 'Phone'),
        ];
    }

    /**
     * Signs user up.
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->phone = $this->phone;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($this->module->emailConfirm) {
            $user->status = User::STATUS_INACTIVE;
            $user->generateEmailVerificationToken();
        } else {
            $user->status = User::STATUS_ACTIVE;
        }

        $transaction = $user->getDb()->beginTransaction();
        if ($user->save()) {
            /**
             * @todo need???
             * @todo if neeed to change email message
             */
            // if ($this->module->sendEmailAfterRegistration) {
            //     $this->sendEmailIsRegistered($user);
            // }
            $profile = new UserProfile();
            $user->link('profile', $profile);

            $transaction->commit();
            return $user;
        }

        return null;
    }

    /**
     * Send email confirm
     * @param User $user
     * @return boolean the sending result
     */
    public function sendEmailConfirm(User $user)
    {
        return $this->module->mail->compose('emailConfirm', ['user' => $user])
            ->setTo($user->email)
            ->setSubject(Yii::t('user', 'E-mail confirmation for') . '. (' . Yii::$app->name . ')')
            ->send();
    }

    /**
     * Send email user is registered
     * @param User $user
     * @return boolean the sending result
     */
    public function sendEmailIsRegistered(User $user)
    {
        return $this->module->mail->compose('registered', ['user' => $user])
            ->setTo($user->email)
            ->setSubject(Yii::t('user', 'Registration on') . '. (' . Yii::$app->name . ')')
            ->send();
    }
}
