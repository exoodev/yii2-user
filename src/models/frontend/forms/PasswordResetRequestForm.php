<?php

namespace exoo\user\models\frontend\forms;

use Yii;
use yii\base\Model;
use exoo\user\models\User;
use exoo\user\components\Mailer;
use exoo\user\traits\ModuleTrait;

/**
 * Password reset request form.
 */
class PasswordResetRequestForm extends Model
{
    use ModuleTrait;
    
    /**
     * @var string User email.
     */
    public $email;
    /**
     * @var User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'exist',
                'targetClass' => User::className(),
                'filter' => function ($query) {
                    $query->active();
                },
                'message' => Yii::t('user', 'There is no user with such e-mail.'),
            ],
            ['email', 'validateSend'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateSend($attribute, $params)
    {
        if (!$this->hasErrors() && $user = $this->getUser()) {
            if (User::isPasswordResetTokenValid($user->password_reset_token)) {
                $this->addError($attribute, Yii::t('user', 'Token has already been sent.'));
            }
        }
    }

    /**
     * @return boolean the result
     */
    public function resetPassword()
    {
        if (!$this->validate()) {
            return false;
        }

        if ($user = $this->getUser()) {
            $user->generatePasswordResetToken();
            if ($user->save()) {
                $this->sendEmail($user);
                return true;
            }
        }

        return false;
    }

    /**
     * Find user by [[email]]
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }
        
        return $this->_user;
    }

    /**
     * Send email
     * @param User $user
     * @return boolean the sending result
     */
    protected function sendEmail(User $user)
    {
        return $this->module->mail->compose('resetPassword', ['user' => $user])
            ->setTo($user->email)
            ->setSubject(Yii::t('user', 'Reset password') . '. (' . Yii::$app->name . ')')
            ->send();
    }
}
