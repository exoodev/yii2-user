<?php

namespace exoo\user;

use Yii;
use yii\helpers\ArrayHelper;
use yii\di\Instance;
use yii\base\InvalidConfigException;
use yii\swiftmailer\Mailer;
use exoo\user\assets\UserStyleAsset;
use yii\authclient\ClientInterface;

class Module extends \yii\base\Module
{
    /**
     * @var boolean
     */
    public $isBackend = false;
    /**
     * @var boolean
     */
    public $loginPhone = true;
    /**
     * @var array
     */
    public $phoneOptions = [
        'country_codes' => [
            'ru' => ['label' => 'Россия', 'mask' => '+7(999) 999-99-99'],
            'kz' => ['label' => 'Казахстан', 'mask' => '+7(999) 999-99-99'],
            'am' => ['label' => 'Армения', 'mask' => '+374-99-999-999'],
            'bl' => ['label' => 'Беларусь', 'mask' => '+375(999)999-99-99'],
            'kg' => ['label' => 'Кыргызстан', 'mask' => '+996(999)999-999'],
            'az' => ['label' => 'Азербайджан', 'mask' => '+994-99-999-99-99'],
            'ge' => ['label' => 'Грузия', 'mask' => '+995(999)999-999'],
            'md' => ['label' => 'Молдова', 'mask' => '+373-9999-9999'],
            'tj' => ['label' => 'Таджикистан', 'mask' => '+992-99-999-9999'],
            'tm' => ['label' => 'Туркмения', 'mask' => '+993-9-999-9999'],
            'uz' => ['label' => 'Узбекистан', 'mask' => '+998-99-999-9999'],
            'ua' => ['label' => 'Украина', 'mask' => '+380(999)999-99-99'],
        ]
    ];
    /**
     * @var integer
     */
    public $passwordResetTokenExpire = 3600;
    /**
     * @var integer
     */
    public $rememberMeDuration = 3600 * 24 * 30;
    /**
     * The auth manager permission to enter the backend.
     *
     * @var string
     */
    public $backendPermission = 'backendIn';
    /**
     * Confirm email after registration.
     *
     * @var boolean
     */
    public $emailConfirm = false;
    /**
     * Send email after registration.
     *
     * @var boolean
     */
    public $sendEmailAfterRegistration = false; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    /**
     * Require Email in account form.
     *
     * @var boolean
     */
    public $emailIsRequiredAccountForm = false;
    /**
     * Allow email change
     *
     * @var boolean
     */
    public $allowEmailChange = false;
    /**
     * The directory that contains the view files for composing mail messages.
     *
     * @var string
     */
    public $viewPathMailer = '@exoo/user/mails';
    /**
     * @var string E-mail address from that will be sent the messages.
     */
    public $robotEmail;
    /**
     * @var string Name of email sender.
     * Default: Yii::$app->name.
     */
    public $robotName;
    /**
     * Url to Privacy Policy
     * @var string
     */
    public $urlPrivacyPolicy;
    /**
     * @var string Name of the file storage application component.
     */
    public $fileStorage = 'fileStorage';
    /**
     * @var BucketInterface bucket instance itself.
     */
    public $fileStorageBucket;
    /**
     * @var string name of the auth client collection application component.
     * This component will be used to fetch services value if it is not set.
     */
    public $clientCollection = 'authClientCollection';
    /**
     * @var array the file buckets.
     */
    public $buckets = [
        'userPhoto' => [
            'baseSubPath' => 'user/photos',
            'fileSubDirTemplate' => '{^name}/{^^name}/{^^^name}',
        ],
        'userFiles' => [
            'baseSubPath' => 'user/files',
            'fileSubDirTemplate' => '{^name}/{^^name}/{^^^name}',
        ],
        'userImages' => [
            'baseSubPath' => 'user/images',
            'fileSubDirTemplate' => '{^name}/{^^name}/{^^^name}',
        ],
    ];
    /**
     * Google reCAPTCHA v3
     * @var boolean
     */
    public $useRecaptcha = false;
    /**
     * Google reCAPTCHA v3 options
     * [
     *     'siteKey' => '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
     *     'secret' => '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe',
     * ]
     * @see https://www.google.com/recaptcha/admin/create
     * @var array
     */
    public $recaptchaOptions = [];
    /**
     * @var integer recording period of the user's last visit in seconds (e.g. 86400 - one day)
     */
    public $periodLastVisit;
    
    /**
     * @var Mailer Mailer instance
     */
    private $_mail;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if ($this->useRecaptcha && empty($this->recaptchaOptions)) {
            throw new InvalidConfigException("The 'recaptchaOptions' option is required.");
        }

        if ($this->isBackend === true) {
            $this->setViewPath('@exoo/user/views/backend');
            if ($this->controllerNamespace === null) {
                $this->controllerNamespace = 'exoo\user\controllers\backend';
            }
        } else {
            $this->setViewPath('@exoo/user/views/frontend');
            if ($this->controllerNamespace === null) {
                $this->controllerNamespace = 'exoo\user\controllers\frontend';
            }
        }

        Yii::$app->mailer->viewPath = $this->viewPathMailer;

        $this->ensureFileStorageBucket();
        $view = Yii::$app->getView();
        UserStyleAsset::register($view);

        parent::init();
    }

    /**
     * @return void|array
     */
    public function getPhoneMasks()
    {
        $codes = ArrayHelper::getValue($this->phoneOptions, 'country_codes');

        if ($codes) {
            return ArrayHelper::getColumn($codes, 'mask', false);
        }
    }

    /**
     * Returns the file storage bucket for the files by name given with [[fileStorageBucket]].
     * If no bucket exists attempts to create it.
     * @return BucketInterface file storage bucket instance.
     */
    public function ensureFileStorageBucket()
    {
        if (!is_object($this->fileStorageBucket)) {
            /* @var StorageInterface $fileStorage */
            $this->fileStorageBucket = Instance::ensure($this->fileStorage, 'exoo\storage\FileStorage');
        }
        $this->fileStorageBucket->setBuckets($this->buckets);
    }

    /**
     * @return Mailer Mailer instance.
     */
    public function getMail()
    {
        if ($this->_mail === null) {
            if (empty($this->robotEmail)) {
                throw new InvalidConfigException("The 'robotEmail' option is required.");
            }
            $this->_mail = Yii::$app->getMailer();
            $this->_mail->viewPath = $this->viewPathMailer;
            if ($this->robotName === null) {
                $this->robotName = Yii::$app->name;
            }
            $this->_mail->messageConfig['from'] = [$this->robotEmail => $this->robotName];
        }
        return $this->_mail;
    }

    /**
     * Returns default auth clients list.
     * @return ClientInterface[] auth clients list.
     */
    public function getAuthClients()
    {
        /** @var $collection \yii\authclient\Collection */
        $collection = Yii::$app->get($this->clientCollection);

        return $collection->getClients();
    }
}
