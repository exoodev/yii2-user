<?php

namespace exoo\user\helpers;

class PhoneHelper
{
    public static function format($value)
    {
        if ($value) {
            return preg_replace('/\D+/', '', $value);
        }
    }
}