<?php

namespace exoo\user\helpers;

use Yii;
use yii\helpers\ArrayHelper;
use exoo\user\models\UserProfile;

class GenderHelper
{
    public static function list()
    {
        return [
            UserProfile::GENDER_FEMALE => Yii::t('user', 'Female'),
            UserProfile::GENDER_MALE => Yii::t('user', 'Male'),
        ];
    }

    public static function name($gender)
    {
        return ArrayHelper::getValue(self::list(), $gender);
    }
}