<?php

namespace exoo\user\helpers;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\user\models\User;

class UserHelper
{
    public static function statusesList()
    {
        return [
            User::STATUS_ACTIVE => Yii::t('user', 'Active'),
            User::STATUS_INACTIVE => Yii::t('user', 'Inactive'),
            User::STATUS_BLOCKED => Yii::t('user', 'Blocked'),
            User::STATUS_DELETED => Yii::t('user', 'Deleted'),
        ];
    }

    public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusesList(), $status);
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case User::STATUS_DELETED:
                $class = 'uk-label uk-label-danger';
                break;
            case User::STATUS_INACTIVE:
                $class = 'uk-label uk-label-warning';
                break;
            case User::STATUS_ACTIVE:
                $class = 'uk-label uk-label-success';
                break;
            case User::STATUS_BLOCKED:
                $class = 'uk-label uk-label-danger';
                break;
            default:
                $class = 'uk-label';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusesList(), $status), [
            'class' => $class,
        ]);
    }
}