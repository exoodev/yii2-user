<?php

namespace exoo\user;

use Yii;
use yii\base\BootstrapInterface;
use exoo\user\models\UserVisit;

class Bootstrap implements BootstrapInterface
{
	/**
     * @inheritdoc
     */
	public function bootstrap($app)
	{
		$app->i18n->translations['user'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'basePath' => '@exoo/user/messages',
		];

		if ($app instanceof \yii\web\Application) {
			$app->on($app::EVENT_BEFORE_ACTION, function () {
				if (!Yii::$app->user->isGuest) {
					$user = Yii::$app->user->identity;
					// $module = Yii::$app->getModule('user');

					// if ($module->periodLastVisit && time() > $user->last_visit + $module->periodLastVisit) {
					// 	$user->logAuth(['duration' => $module->periodLastVisit]);
					// }
					$user->touch('last_visit');
				}
			});
        }
	}
}
