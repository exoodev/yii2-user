<?php

use yii\helpers\Html;
use exoo\grid\GridView;
use exoo\user\helpers\UserHelper;
use yii\widgets\MaskedInputAsset;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $searchModel exoo\user\models\backend\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

MaskedInputAsset::register($this);
$this->title = Yii::t('user', 'Users');
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
    'buttons' => [
        Html::a('Создать', ['create'], [
            'class' => 'uk-button uk-button-primary',
            'data-pjax' => 0
        ]),
        Html::a(Yii::t('user', 'Delete'), ['batch-delete'], [
            'class' => 'uk-button uk-button-danger uk-hidden',
            'data' => [
                'method' => 'post',
                'confirm' => Yii::t('user', 'Are you sure you want to delete the selected items?'),
            ],
            'grid-selected' => true
        ]),
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'exoo\grid\CheckboxColumn'],

        [
            'attribute' => 'id',
            'headerOptions' => ['class' => 'uk-table-shrink'],
            'format' => 'html',
            'value' => function($model) {
                return Html::a(Html::encode($model->id), ['update', 'id' => $model->id]);
            },
        ],
        'email:email',
        'username',
        [
            'attribute' => 'phone',
            'format' => 'raw',
            'value' => function($model) {
                $phone = Html::encode($model->phone);
                return Html::a($phone, 'tel:+' . $phone, ['class' => 'user-phone']);
            },
        ],
        'created_at:datetime',
        'last_visit:datetime',
        [
            'attribute' => 'status',
            'format' => 'html',
            'filter' => UserHelper::statusesList(),
            'value' => function($model) {
                return UserHelper::statusLabel($model->status);
            },
        ],
        [
            'class' => 'exoo\grid\ActionColumn',
            'template' => '{update} {delete}',
            'visibleButtons' => [
                'delete' => function ($model) {
                    return $model->isNotDeleted();
                },
            ]
        ],
    ],
]) ?>

<?php
$masks = Json::htmlEncode($this->context->module->phoneMasks);
$this->registerJs("$('.user-phone').inputmask({ mask: $masks })");
