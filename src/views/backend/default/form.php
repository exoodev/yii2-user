<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\user\helpers\UserHelper;
use yii\widgets\MaskedInput;
use exoo\uikit\DetailView;

/* @var $this yii\web\View */
/* @var $user exoo\user\models\User */
/* @var $model exoo\user\models\backend\UserForm */
/* @var $form exoo\uikit\ActiveForm */

$this->title = Yii::t('user', 'User');

if (isset($user)) {
    $this->title .= Html::tag('span', 'ID: ' . $user->id, ['class' => 'uk-label uk-margin-small-left']);
}
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <?php $form = ActiveForm::begin(); ?>
        <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
            <div>
                <h3 class="uk-card-title uk-margin-remove"><?= $this->title ?></h3>
            </div>
            <div>
                <?= Html::submitButton(Yii::t('user', 'Save'), [
                    'class' => 'uk-button uk-button-primary',
                ]) ?>
                <?= Html::a(Yii::t('user', 'Back'), Yii::$app->user->returnUrl, [
                    'class' => 'uk-button uk-button-default',
                ]) ?>
            </div>
        </div>
        
        <div uk-grid>
            <div class="uk-width-expand@m">
                <?= $form->field($model, 'email')->textInput([
                    'maxlength' => true,
                    'autocomplete' => 'email',
                    'type' => 'email',
                ]) ?>
                
                <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                    'mask' => $this->context->module->phoneMasks,
                    'options' => [
                        'autocomplete' => 'tel',
                        'type' => 'tel',
                    ]
                ]) ?>

                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'status')->dropDownList(UserHelper::statusesList()) ?>
            </div>
            <div class="uk-width-1-3@m">
                <?php if (isset($user)): ?>
                    <label class="uk-form-label">Дополнительная информация</label>
                    <?= DetailView::widget([
                        'model' => $user,
                        'options' => ['class' => 'uk-table uk-table-divider uk-table-small uk-table-justify'],
                        'attributes' => [
                            'last_visit:datetime',
                            'created_at:datetime',
                            'updated_at:datetime',
                        ],
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
