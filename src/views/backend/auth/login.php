<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\user\assets\UserAsset;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $user exoo\user\models\User */
/* @var $model exoo\user\models\LoginForm */
/* @var $form exoo\uikit\ActiveForm */

$this->title = Yii::t('user', 'Login');
UserAsset::register($this);
?>
<div class="uk-flex uk-flex-center uk-flex-middle uk-height-1-1" user-login>
    <div class="uk-card uk-card-default uk-card-body uk-card-large">

        <h3 class="uk-card-title uk-text-center"><?= Html::encode($this->title) ?></h3>
        
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'validateOnBlur' => false
        ]); ?>

            <?php if ($this->context->module->loginPhone): ?>
                <ul class="uk-subnav uk-subnav-pill" uk-switcher>
                    <li><a href="#" data-name="phone"><?= $model->getAttributeLabel('phone') ?></a></li>
                    <li><a href="#" data-name="email">E-mail</a></li>
                </ul>
                <?= $form->field($model, 'entity')->textInput([
                    'class' => 'uk-width-1-1 js-user-entity',
                    'autocomplete' => 'tel',
                    'type' => 'tel',
                ])->label(false) ?>
                <?php
                \yii\widgets\MaskedInputAsset::register($this);
                $masks = Json::htmlEncode($this->context->module->phoneMasks);
                $this->registerJs("User.loginMask($masks)"); 
                ?>
            <?php else: ?>
                <?= $form->field($model, 'entity')->textInput([
                    'class' => 'uk-width-1-1',
                    'placeholder' => $model->getAttributeLabel('entity'),
                    'autocomplete' => 'email',
                    'type' => 'email',
                ])->label(false) ?>
            <?php endif; ?>

            <?= $form->field($model, 'password')->passwordInput([
                'class' => 'uk-width-1-1',
                'placeholder' => $model->getAttributeLabel('password')
            ])->label(false) ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="uk-form-row">
                <?= Html::submitButton(Yii::t('user', 'Login'), [
                    'class' => 'uk-button uk-button-primary uk-button-large uk-width-large uk-text-center',
                    'name' => 'login-button'
                ]) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>