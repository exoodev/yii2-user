<?php
use yii\helpers\Html;

use exoo\uikit\UikitAsset;

UikitAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="uk-height-1-1">
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
	</head>
	<body class="uk-height-1-1">
	<?php $this->beginBody() ?>
		<?= $content ?>
	<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>
