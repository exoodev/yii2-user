<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

$this->title = Yii::t('user', 'Enter your e-mail');
?>
<div class="uk-flex uk-flex-center uk-flex-middle">
    <div class="uk-card uk-card-default uk-card-body uk-card-large uk-width-xlarge">

        <h1 class="uk-h2"><?= Html::encode($this->title) ?></h1>

        <?php $form = ActiveForm::begin(['id' => 'addEmailForm']); ?>

            <?= $form->field($model, 'email')->textInput([
                'placeholder' => $model->getAttributeLabel('email'),
                'autocomplete' => 'email',
                'type' => 'email',
            ])->label(false) ?>

            <p class="uk-form-row">
                <?= Html::submitButton(Yii::t('user', 'Save'), [
                    'class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1',
                ]) ?>
            </p>

        <?php ActiveForm::end(); ?>

    </div>
</div>

