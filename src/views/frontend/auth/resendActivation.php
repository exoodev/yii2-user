<?php
use yii\helpers\Html;
use exoo\uikit\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \exoo\user\models\frontend\forms\ResendEmailConfirmForm */

$this->title = Yii::t('user', 'Resend activation');
?>

<div class="uk-flex uk-flex-center uk-flex-middle">
    <div class="uk-card uk-card-default uk-card-body uk-card-large uk-width-xlarge">

        <h1 class="uk-h2"><?= Html::encode($this->title) ?></h1>
        <p><?= Yii::t('user', 'Enter your Email to resend activation') ?></p>

        <?php $form = ActiveForm::begin([
            'id' => 'resend-activation-form'
        ]); ?>
            <?= $form->field($model, 'email')->textInput([
                'placeholder' => 'E-mail',
                'autocomplete' => 'email',
                'type' => 'email',
            ])->label(false) ?>

            <div class="uk-form-row">
                <?= Html::submitButton(Yii::t('user', 'Send'), [
                    'class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1'
                ]) ?>
            </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>
