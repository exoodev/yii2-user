<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\user\assets\UserAsset;
use yii\helpers\Json;
use yii\widgets\MaskedInputAsset;

/* @var $this yii\web\View */
/* @var $user exoo\user\models\User */
/* @var $model exoo\user\models\LoginForm */
/* @var $form exoo\uikit\ActiveForm */

$this->title = Yii::t('user', 'Login with password');
UserAsset::register($this);
?>
<div class="uk-flex uk-flex-center uk-flex-middle" user-login>
    <div class="uk-card uk-card-default uk-card-body uk-card-large uk-width-xlarge">

        <ul class="uk-tab">
            <li class="uk-active"><a href="#"><?= Yii::t('user', 'Login') ?></a></li>
            <li><?= Html::a(Yii::t('user', 'Signup'), ['signup']) ?></li>
        </ul>
        
        <?php if ($this->context->module->getAuthClients()): ?>
            <h3 class="uk-h4 uk-text-center"><?= Yii::t('user', 'Login using social networks') ?></h3>
            <?= $this->render('_auth-choice') ?>
            <div class="uk-text-center uk-margin"><?= Yii::t('user', 'or') ?></div>
        <?php else: ?>
            <h3 class="uk-card-title uk-text-center"><?= Html::encode($this->title) ?></h3>
        <?php endif; ?>
        
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'validateOnBlur' => false
        ]); ?>

            <?php if ($this->context->module->loginPhone): ?>
                <ul class="uk-subnav uk-subnav-pill" uk-switcher>
                    <li><a href="#" data-name="phone"><?= $model->getAttributeLabel('phone') ?></a></li>
                    <li><a href="#" data-name="email">E-mail</a></li>
                </ul>
                <?= $form->field($model, 'entity')->textInput([
                    'class' => 'js-user-entity',
                    'autocomplete' => 'tel',
                    'type' => 'tel',
                ])->label(false) ?>
                <?php
                MaskedInputAsset::register($this);
                $masks = Json::htmlEncode($this->context->module->phoneMasks);
                $this->registerJs("User.loginMask($masks)"); 
                ?>
            <?php else: ?>
                <?= $form->field($model, 'entity')->textInput([
                    'placeholder' => $model->getAttributeLabel('entity'),
                    'autocomplete' => 'email',
                    'type' => 'email',
                ])->label(false) ?>
            <?php endif; ?>

            <?= $form->field($model, 'password')->passwordInput([
                'placeholder' => $model->getAttributeLabel('password')
            ])->label(false) ?>

            <div class="uk-flex uk-flex-between uk-text-small">
                <div><?= $form->field($model, 'rememberMe')->checkbox() ?></div>
                <div><?= Html::a(Yii::t('user', 'Forgot your password?'), ['request-password-reset']) ?></div>
            </div>

            <?= Html::submitButton(Yii::t('user', 'Login'), [
                'class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1 uk-text-center',
                'name' => 'login-button'
            ]) ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>