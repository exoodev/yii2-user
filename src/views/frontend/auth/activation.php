<?php

use yii\helpers\Html;

$this->title = Yii::t('user', 'Activate e-mail');

$url = ['resend-activation'];
$link = Yii::t('user', 'Check your email');

if ($email = Html::encode($email)) {
    $domain = array_pop(explode('@', $email));
    $link = Html::a($link, 'http://' . $domain, ['target' => '_blank']);
    $url = ['resend-activation', 'email' => $email];
}
?>

<div class="uk-flex uk-flex-center uk-flex-middle">
    <div class="uk-card uk-card-default uk-card-body uk-card-large uk-width-xlarge uk-text-center">

        <i class="uk-text-muted" uk-icon="mail" ratio="3"></i>
        <h1 class="uk-margin"><?= Html::encode($this->title) ?></h1>
        <div class="uk-margin"><?= Yii::t('user', 'To complete the registration you need to activate e-mail. {link} and click the link in the email.', ['link' => $link]) ?></div>
        <div class="uk-margin-bottom uk-h1"><?= Html::encode($email) ?></div>
        <hr>
        <div class="uk-text-small"><?= Html::a(Yii::t('user', 'Re-send an email with the activation'), $url) ?></div>

    </div>
</div>
