<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

$this->title = Yii::t('user', 'Reset password');
?>
<div class="uk-flex uk-flex-center uk-flex-middle">
    <div class="uk-card uk-card-default uk-card-body uk-card-large uk-width-xlarge">

        <h1 class="uk-h2"><?= Html::encode($this->title) ?></h1>
        <p><?= Yii::t('user', 'Please fill out your e-mail. A link to reset password will be sent there.') ?></p>
        
        <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'email')->textInput([
                'placeholder' => $model->getAttributeLabel('email'),
                'autocomplete' => 'email',
                'type' => 'email',
            ])->label(false) ?>
            
            <div class="uk-form-row">
                <?= Html::submitButton(Yii::t('user', 'Send'), ['class' => 'uk-button uk-button-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>
