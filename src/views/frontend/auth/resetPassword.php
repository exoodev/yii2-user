<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\user\assets\UserAsset;

UserAsset::register($this);
$this->title = Yii::t('user', 'Reset password');
?>
<div class="uk-flex uk-flex-center uk-flex-middle">
    <div class="uk-card uk-card-default uk-card-body uk-card-large uk-width-xlarge">

        <h1 class="uk-h2"><?= Html::encode($this->title) ?></h1>
        <p><?= Yii::t('user', 'Create a new password') ?></p>

        <?php $form = ActiveForm::begin(); ?>
            <?= $form
                ->field($model, 'password')
                ->passwordInput([
                    'toggle' => true,
                    'placeholder' => $model->getAttributeLabel('password'),
                ])
                ->label(false)
                ->icon('refresh', [
                    'url' => '#',
                    'flip' => true,
                    'onclick' => "User.setPassword(event)",
                    'uk-tooltip' => Yii::t('user', 'Generate password')
                ]) ?>

            <div class="uk-form-row">
                <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
