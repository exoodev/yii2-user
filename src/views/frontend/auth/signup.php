<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;
use exoo\uikit\ActiveForm;
use exoo\user\assets\UserAsset;
use himiklab\yii2\recaptcha\ReCaptcha3;

$this->title = Yii::t('user', 'Signup');
$activation = Html::a(Yii::t('user', 'activation'), ['resend-activation']);
UserAsset::register($this);
?>
<div class="uk-flex uk-flex-center uk-flex-middle">
    <div class="uk-card uk-card-default uk-card-body uk-card-large uk-width-xlarge">
        
        <ul class="uk-tab">
            <li><?= Html::a(Yii::t('user', 'Login'), ['login']) ?></li>
            <li class="uk-active"><a href="#"><?= Yii::t('user', 'Signup') ?></a></li>
        </ul>

        <?php if ($this->context->module->getAuthClients()): ?>
            <h3 class="uk-h4 uk-text-center"><?= Yii::t('user', 'Sign up using social networks') ?></h3>
            <?= $this->render('_auth-choice') ?>
            <div class="uk-text-center uk-margin"><?= Yii::t('user', 'or') ?></div>
        <?php else: ?>
            <h3 class="uk-card-title"><?= Html::encode($this->title) ?></h3>
        <?php endif; ?>

        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'username')->textInput([
                'placeholder' => $model->getAttributeLabel('username'),
                'autocomplete' => 'name',
            ])->label(false) ?>

            <?= $form->field($model, 'email')->textInput([
                'placeholder' => $model->getAttributeLabel('email'),
                'autocomplete' => 'email',
                'type' => 'email',
            ])->label(false) ?>

            <?php if ($this->context->module->loginPhone): ?>
                <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                    'mask' => $this->context->module->phoneMasks,
                    'options' => [
                        'placeholder' => $model->getAttributeLabel('phone'),
                        'autocomplete' => 'tel',
                        'type' => 'tel',
                    ]
                ])->label(false) ?>
            <?php endif; ?>

            <?= $form
                ->field($model, 'password')->passwordInput([
                    'toggle' => true,
                    'placeholder' => $model->getAttributeLabel('password'),
                ])
                ->label(false)
                ->icon('refresh', [
                    'url' => '#',
                    'flip' => true,
                    'onclick' => "User.setPassword(event)",
                    'uk-tooltip' => Yii::t('user', 'Generate password')
                ])
            ?>

            <?php if ($this->context->module->useRecaptcha): ?>
            <?= $form->field($model, 'recaptcha', ['enableAjaxValidation' => false])->widget(ReCaptcha3::className(), [
                'siteKey' => ArrayHelper::getValue($this->context->module->recaptchaOptions, 'siteKey'),
                'action' => null,
            ])->label(false) ?>
            <?php endif; ?>

            <div class="uk-margin">
                <?= Html::submitButton(Yii::t('user', 'Signup'), [
                    'class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1',
                ]) ?>
            </div>

            <?php if ($this->context->module->emailConfirm): ?>
                <p class="uk-text-center">
                    <?= Yii::t('user', 'Did you miss your {activation} email?', ['activation' => $activation]) ?>
                </p>
            <?php endif; ?>

            <?php if ($url = $this->context->module->urlPrivacyPolicy): ?>
                <?= $form->field($model, 'privacyPolicy')->checkbox([
                    'label' => Yii::t('user', 'I have read the {agreement} on the processing of personal data and accept its terms.', [
                        'agreement' => Html::a(Yii::t('user', 'agreement'), [$url], ['target' => '_blank'])
                    ]),
                    'labelOptions' => ['class' => 'uk-text-small'],
                    'class' => 'uk-margin-small-right',
                ]) ?>
            <?php endif; ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
