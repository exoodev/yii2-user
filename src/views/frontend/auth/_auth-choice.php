<?php
use yii\authclient\widgets\AuthChoice;

?>

<?php $authAuthChoice = AuthChoice::begin([
    'baseAuthUrl' => ['client'],
    'popupMode' => false,
]); ?>
    <div class="uk-flex uk-flex-center">
        <?php foreach ($authAuthChoice->getClients() as $client): ?>
            <div class="uk-margin-small-right"><?= $authAuthChoice->clientLink($client, null, ['uk-tooltip' => true]) ?></div>
        <?php endforeach; ?>
    </div>
<?php AuthChoice::end(); ?>

 