<?php
use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use yii\widgets\MaskedInput;
use exoo\user\helpers\GenderHelper;
use exoo\user\widgets\SocialManager;
use exoo\user\assets\UserAsset;

UserAsset::register($this);

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $user \exoo\user\models\User */
/* @var $model \exoo\user\models\frontend\forms\ProfileForm */

$this->title = Yii::t('user', 'Profile');
?>

<div class="uk-card uk-card-default">
        <div class="uk-card-header">
            <h1 class="uk-card-title"><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="uk-card-body">
            <?php $form = ActiveForm::begin([
                'id' => 'profile',
                'layout' => 'horizontal'
            ]); ?>

                <div class="uk-child-width-expand@m" uk-grid>
                    <div>
                        <!-- <div class="uk-inline-clip uk-transition-toggle uk-light uk-text-center" tabindex="0">
                            <div class="user-profile-avatar user-default-avatar uk-border-circle"></div>
                            <div class="uk-position-center">
                                <div class="uk-transition-slide-top-small"><a href="#" class="uk-margin-remove">Изменить</a></div>
                                <div class="uk-transition-slide-bottom-small"><a href="#" class="uk-margin-remove">Удалить</a></div>
                            </div>
                        </div> -->

                        <?= $form->field($model, 'email')->textInput([
                            'disabled' => !$model->allowEmailChange(),
                            'autocomplete' => 'email',
                            'type' => 'email',
                        ]) ?>

                        <?= $form->field($model, 'username')->textInput([
                            'autocomplete' => 'name',
                        ]) ?>

                        <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                            'mask' => $this->context->module->phoneMasks,
                            'options' => [
                                'autocomplete' => 'tel',
                                'type' => 'tel',
                            ]
                        ]) ?>
                        <?= $form->field($model, 'birthday')->textInput([
                            'type' => 'date',
                            'autocomplete' => 'bday',
                        ]) ?>
                        <?= $form->field($model, 'gender')->inline()->radioList(GenderHelper::list(), [
                            'itemOptions' => [
                                'autocomplete' => 'sex',
                            ],
                        ]) ?>
                        <?php if ($url = $this->context->module->urlPrivacyPolicy): ?>
                            <div class="uk-margin-large">
                                <?php $form->layout = 'stacked'; ?>
                                <?= $form->field($model, 'privacyPolicy')->checkbox([
                                    'label' => Yii::t('user', 'I have read the {agreement} on the processing of personal data and accept its terms.', [
                                        'agreement' => Html::a(Yii::t('user', 'agreement'), [$url], ['target' => '_blank'])
                                    ]),
                                ]) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div>
                        <?= $form->field($model, 'first_name')->textInput([
                            'autocomplete' => 'first name',
                        ]) ?>
                        <?= $form->field($model, 'last_name')->textInput([
                            'autocomplete' => 'last name',
                        ]) ?>
                        <?= $form->field($model, 'country')->textInput([
                            'autocomplete' => 'country',
                        ]) ?>

                        <?= $form->field($model, 'city')->textInput([
                            'autocomplete' => 'city',
                        ]) ?>

                        <?= $form->field($model, 'address')->textInput([
                            'autocomplete' => 'address',
                        ]) ?>

                        <hr>

                        <div class="uk-margin">
                            <label class="uk-form-label"><?= Yii::t('user', 'Registration date') ?></label>
                            <div class="uk-form-controls uk-form-controls-text"><?= Yii::$app->formatter->asDatetime($user->created_at) ?></div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label"><?= Yii::t('user', 'Last visit') ?></label>
                            <div class="uk-form-controls uk-form-controls-text"><?= Yii::$app->formatter->asDatetime($user->last_visit) ?>
                            </div>
                        </div>
                    </div>
                </div>
            

                <?= Html::submitButton(Yii::t('user', 'Save'), [
                    'class' => 'uk-button uk-button-primary uk-margin-top',
                ]) ?>
            
            <?php ActiveForm::end(); ?>

        </div>
        <div class="uk-card-footer">
            <h3><?= Yii::t('user', 'Snapping social networks') ?></h3>

            <?= SocialManager::widget([
                'baseAuthUrl' => ['/user/auth/client'],
                'popupMode' => false,
            ]) ?>
        </div>
</div>

