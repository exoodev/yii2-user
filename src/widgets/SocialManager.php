<?php

namespace exoo\user\widgets;

use Yii;
use yii\authclient\widgets\AuthChoice;
use exoo\user\models\Auth;
use yii\helpers\Html;
use yii\helpers\Url;

class SocialManager extends AuthChoice
{
    /**
     * @var array
     */
    public $itemsOptions = ['class' => 'uk-flex'];
    /**
     * @var array
     */
    public $itemOptions = [];

    private $_userClients;

    public function getUserClients()
    {
        if ($this->_userClients === null) {
            $this->_userClients = Auth::find()
                ->where(['user_id' => Yii::$app->user->id])
                ->all();
        }

        return $this->_userClients;
    }

    public function authExists($source)
    {
        foreach ($this->getUserClients() as $client) {
            if ($source == $client->source) {
                return true;
            }
        }
    }
    
    /**
     * Renders the main content, which includes all external services links.
     * @return string generated HTML.
     */
    protected function renderMainContent()
    {
        $items = [];
        Html::addCssClass($this->itemOptions, 'auth-item');

        foreach ($this->getClients() as $externalService) {
            $itemOptions = $this->itemOptions;
            if ($this->authExists($externalService->id)) {
                Html::addCssClass($itemOptions, 'active');
            }
            $link = $this->clientLink($externalService) . "\n";
            $remove = Html::tag('span', null, [
                'class' => 'auth-remove',
                'data-client' => $externalService->id,
                'uk-tooltip' => true,
                'title' => Yii::t('user', 'Delete') . '?',
                'delay' => 500
            ]);
            $items[] = Html::tag('div', $link . $remove, $itemOptions);
        }

        return Html::tag('div', implode('', $items), $this->itemsOptions);
    }

    /**
     * Outputs client auth link.
     * @param ClientInterface $client external auth client instance.
     * @param string $text link text, if not set - default value will be generated.
     * @param array $htmlOptions link HTML options.
     * @return string generated HTML.
     * @throws InvalidConfigException on wrong configuration.
     */
    public function clientLink($client, $text = null, array $htmlOptions = [])
    {
        
        $htmlOptions['uk-tooltip'] = true;

        return parent::clientLink($client, $text, $htmlOptions);
    }

    /**
     * Composes client auth URL.
     * @param ClientInterface $client external auth client instance.
     * @return string auth URL.
     */
    public function createClientUrl($client, $params = [])
    {
        $this->autoRender = false;
        $url = $this->getBaseAuthUrl();
        $url[$this->clientIdGetParamName] = $client->getId();
        $url['successUrl'] = Url::to();

        return Url::to(array_merge($url, $params));
    }
}