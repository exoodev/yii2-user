<?php

namespace exoo\user\components;

use exoo\user\models\Auth;
use exoo\user\models\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;
use exoo\user\models\UserProfile;
use exoo\user\traits\ModuleTrait;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    use ModuleTrait;

    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();
        $email = ArrayHelper::getValue($attributes, 'email');
        $id = ArrayHelper::getValue($attributes, 'id');
        $username = $this->getUsername($attributes);

        /* @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $id,
        ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                /* @var User $user */
                $user = $auth->user;
                Yii::$app->user->login($user, $this->getModule()->rememberMeDuration);
            } else { // signup
                if ($email !== null && User::find()->where(['email' => $email])->exists()) {
                    Yii::$app->getSession()->setFlash('alert.error', [
                        Yii::t('user', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $this->client->getTitle()]),
                    ]);
                } else {
                    $user = new User([
                        'username' => $username,
                        'email' => $email,
                    ]);
                    $user->asActive();
                    $user->generateAuthKey();
                    $user->generatePasswordResetToken();

                    $transaction = User::getDb()->beginTransaction();

                    if ($user->save()) {
                        $this->linkProfile($user);
                        $auth = new Auth([
                            'user_id' => $user->id,
                            'source' => $this->client->getId(),
                            'source_id' => (string) $id,
                        ]);
                        if ($auth->save()) {
                            $transaction->commit();
                            Yii::$app->user->login($user, $this->getModule()->rememberMeDuration);
                        } else {
                            Yii::$app->getSession()->setFlash('alert.error', [
                                Yii::t('app', 'Unable to save {client} account: {errors}', [
                                    'client' => $this->client->getTitle(),
                                    'errors' => json_encode($auth->getErrors()),
                                ]),
                            ]);
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('alert.error', [
                            Yii::t('app', 'Unable to save user: {errors}', [
                                'client' => $this->client->getTitle(),
                                'errors' => json_encode($user->getErrors()),
                            ]),
                        ]);
                    }
                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $this->client->getId(),
                    'source_id' => (string) $attributes['id'],
                ]);
                if ($auth->save()) {
                    /** @var User $user */
                    $user = $auth->user;
                    Yii::$app->getSession()->setFlash('alert.success', [
                        Yii::t('user', 'Linked {client} account.', [
                            'client' => $this->client->getTitle()
                        ]),
                    ]);
                } else {
                    Yii::$app->getSession()->setFlash('alert.error', [
                        Yii::t('user', 'Unable to link {client} account: {errors}', [
                            'client' => $this->client->getTitle(),
                            'errors' => json_encode($auth->getErrors()),
                        ]),
                    ]);
                }
            } elseif ($auth->user_id !== Yii::$app->user->id) { // there's existing auth
                Yii::$app->getSession()->setFlash('alert.error', [
                    Yii::t(
                        'user',
                        'Unable to link {client} account. There is another user using it.',
                        ['client' => $this->client->getTitle()]
                    ),
                ]);
            }
        }
    }

    /**
     * @param User $user
     */
    private function linkProfile(User $user)
    {
        $userAttributes = $this->client->getUserAttributes();
        $profile = new UserProfile();
        $attributeNames = [
            'first_name',
            'last_name',
            'gender',
            'photo',
            'birthday',
            'country',
            'city',
            'address',
        ];

        foreach ($attributeNames as $attribute) {
            $value = ArrayHelper::getValue($userAttributes, $attribute);
            if ($value) {
                $profile->{$attribute} = $value;
            };
        }
        $user->link('profile', $profile);
    }

    /**
     * @param array $attributes
     * @return string
     */
    private function getUsername($attributes)
    {
        if ($username = ArrayHelper::getValue($attributes, 'username')) {
            return $username;
        }
        if ($first_name = ArrayHelper::getValue($attributes, 'first_name')) {
            $username = $first_name;
        }
        if ($last_name = ArrayHelper::getValue($attributes, 'last_name')) {
            $username .= ' ' . $last_name;
        }

        return $username;
    }
}
