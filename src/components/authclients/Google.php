<?php

/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\user\components\authclients;

use Yii;
use yii\helpers\ArrayHelper;
use exoo\system\models\Profile;

/**
 * Google allows authentication via Google OAuth.
 *
 * In order to use Google OAuth you must create a project at <https://console.developers.google.com/project>
 * and setup its credentials at <https://console.developers.google.com/project/[yourProjectId]/apiui/credential>.
 * In order to enable using scopes for retrieving user attributes, you should also enable Google+ API at
 * <https://console.developers.google.com/project/[yourProjectId]/apiui/api/plus>
 *
 * Example application configuration:
 *
 * ```php
 * 'components' => [
 *     'authClientCollection' => [
 *         'class' => 'yii\authclient\Collection',
 *         'clients' => [
 *             'google' => [
 *                 'class' => 'yii\authclient\clients\Google',
 *                 'clientId' => 'google_client_id',
 *                 'clientSecret' => 'google_client_secret',
 *             ],
 *         ],
 *     ]
 *     // ...
 * ]
 * ```
 *
 * @see https://console.developers.google.com/project
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Google extends \yii\authclient\clients\Google
{
    /**
     * @inheritdoc
     */
    public function getUserAttributes()
    {
        $attributes = parent::getUserAttributes();
        $result = [];

        if (isset($attributes['id'])) {
            $result['id'] = $attributes['id'];
        }

        if (isset($attributes['displayName'])) {
            $result['username'] = $attributes['displayName'];
        }

        $result['email'] = ArrayHelper::getValue($attributes, 'emails.0.value');
        $result['first_name'] = ArrayHelper::getValue($attributes, 'name.givenName');
        $result['last_name'] = ArrayHelper::getValue($attributes, 'name.familyName');

        if (isset($attributes['gender'])) {
            switch ($attributes['gender']) {
                case 'male':
                    $result['gender_id'] = Profile::GENDER_MAN;
                    break;
                case 'female':
                    $result['gender_id'] = Profile::GENDER_WOMAN;
                    break;
                default:
                    $result['gender_id'] = Profile::GENDER_NOT_SPECIFIED;
                    break;
            }
        }

        if (isset($attributes['image.url'])) {
            $result['avatar_url'] = strtok($attributes['image.url'], '?');
        }

        return $result;
    }
}
