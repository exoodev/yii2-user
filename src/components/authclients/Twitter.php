<?php

namespace exoo\user\components\authclients;

/**
 * Twitter allows authentication via Twitter OAuth.
 *
 * In order to use Twitter OAuth you must register your application at <https://dev.twitter.com/apps/new>.
 *
 * Example application configuration:
 *
 * ```php
 * 'components' => [
 *     'authClientCollection' => [
 *         'class' => 'yii\authclient\Collection',
 *         'clients' => [
 *             'twitter' => [
 *                 'class' => 'yii\authclient\clients\Twitter',
 *                 'attributeParams' => [
 *                     'include_email' => 'true'
 *                 ],
 *                 'consumerKey' => 'twitter_consumer_key',
 *                 'consumerSecret' => 'twitter_consumer_secret',
 *             ],
 *         ],
 *     ]
 *     // ...
 * ]
 * ```
 *
 * > Note: some auth workflows provided by Twitter, such as [application-only authentication](https://dev.twitter.com/oauth/application-only),
 *   uses OAuth 2 protocol and thus are impossible to be used with this class. You should use [[TwitterOAuth2]] for these.
 *
 * @see TwitterOAuth2
 * @see https://apps.twitter.com/
 * @see https://dev.twitter.com/
 */
class Twitter extends \yii\authclient\clients\Twitter
{
    /**
     * @inheritdoc
     */
    public $attributeParams = [
        'include_email' => 'true',
    ];

    /**
     * @inheritdoc
     */
    public function getUserAttributes()
    {
        $attributes = parent::getUserAttributes();

        return [
            'id' => $attributes['id'],
            'username' => $attributes['name'],
            'email' => $attributes['email'],
            'photo' => str_replace('_normal', '', $attributes['profile_image_url']),
        ];
    }
}
