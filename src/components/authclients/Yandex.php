<?php

/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\user\components\authclients;

use Yii;
use yii\helpers\ArrayHelper;
use exoo\system\models\Profile;

/**
 * Yandex allows authentication via Yandex OAuth.
 *
 * In order to use Yandex OAuth you must register your application at <https://oauth.yandex.ru/client/new>.
 *
 * Example application configuration:
 *
 * ```php
 * 'components' => [
 *     'authClientCollection' => [
 *         'class' => 'yii\authclient\Collection',
 *         'clients' => [
 *             'yandex' => [
 *                 'class' => 'yii\authclient\clients\Yandex',
 *                 'clientId' => 'yandex_client_id',
 *                 'clientSecret' => 'yandex_client_secret',
 *             ],
 *         ],
 *     ]
 *     // ...
 * ]
 * ```
 *
 * @see https://oauth.yandex.ru/client/new
 * @see http://api.yandex.ru/login/doc/dg/reference/response.xml
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Yandex extends \yii\authclient\clients\Yandex
{
    /**
     * @var string
     */
    public $sizeAvatar = 'islands-200';

    /**
     * @inheritdoc
     */
    public function getUserAttributes()
    {
        $attributes = parent::getUserAttributes();
        $result = [];

        if (isset($attributes['id'])) {
            $result['id'] = $attributes['id'];
        }

        if (isset($attributes['login'])) {
            $result['username'] = $attributes['login'];
        }

        if (isset($attributes['default_email'])) {
            $result['email'] = $attributes['default_email'];
        }

        if (isset($attributes['first_name'])) {
            $result['first_name'] = $attributes['first_name'];
        }

        if (isset($attributes['last_name'])) {
            $result['last_name'] = $attributes['last_name'];
        }

        if (isset($attributes['birthday'])) {
            $result['birthday'] = Yii::$app->formatter->asTimestamp($attributes['birthday']);
        }

        if (isset($attributes['sex'])) {
            switch ($attributes['sex']) {
                case 'male':
                    $result['gender_id'] = Profile::GENDER_MAN;
                    break;
                case 'female':
                    $result['gender_id'] = Profile::GENDER_WOMAN;
                    break;
                default:
                    $result['gender_id'] = Profile::GENDER_NOT_SPECIFIED;
                    break;
            }
        }

        if (!empty($attributes['id'])) {
            $result['avatar_url'] = 'https://avatars.yandex.net/get-yapic/' . $attributes['id'] . '/' . $this->sizeAvatar;
        }

        return $result;
    }
}
