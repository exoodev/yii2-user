<?php

namespace exoo\user\components\authclients;

use exoo\user\models\UserProfile;

/**
 * VKontakte allows authentication via VKontakte OAuth.
 *
 * In order to use VKontakte OAuth you must register your application at <http://vk.com/editapp?act=create>.
 *
 * Example application configuration:
 *
 * ```php
 * 'components' => [
 *     'authClientCollection' => [
 *         'class' => 'yii\authclient\Collection',
 *         'clients' => [
 *             'vkontakte' => [
 *                 'class' => 'exoo\user\components\authclients\VKontakte',
 *                 'clientId' => 'vkontakte_client_id',
 *                 'clientSecret' => 'vkontakte_client_secret',
 *             ],
 *         ],
 *     ]
 *     // ...
 * ]
 * ```
 *
 * @see http://vk.com/editapp?act=create
 * @see http://vk.com/developers.php?oid=-1&p=users.get
 * 
 */
class VKontakte extends \yii\authclient\clients\VKontakte
{
    /**
     * @inheritdoc
     */
    public $scope = ['email'];
    /**
     * @inheritdoc
     */
    public $attributeNames = [
        'uid',
        'first_name',
        'last_name',
        'nickname',
        'screen_name',
        'sex',
        'bdate',
        'photo_max_orig',
        'email',
    ];

    /**
     * @inheritdoc
     */
    public function getUserAttributes()
    {
        $attributes = parent::getUserAttributes();
        $date = str_replace('.', '-', $attributes['bdate']);

        return [
            'id' => $attributes['id'],
            'username' => $attributes['nickname'],
            'first_name' => $attributes['first_name'],
            'last_name' => $attributes['last_name'],
            'gender' => $attributes['sex'] === 1 ? UserProfile::GENDER_FEMALE : UserProfile::GENDER_MALE,
            'photo' => $attributes['photo_max_orig'],
            'birthday' => date("Y-m-d", strtotime($date)),
            'email' => $attributes['email'],
        ];
    }
}
